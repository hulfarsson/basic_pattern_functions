from sage.combinat.permutation import to_standard, Permutation_class

def G(w):
	"""
	The graph/diagram of the permutation w
	"""
	return [ (x+1,y) for (x,y) in enumerate(w) ]
	
def G_inv(w):
	return [ a for _,a in w ]
	
def shift_and_lift(col, row, points):
	new_points = set()
	for a,b in points:
		new_points.add( (a + (a >= col), b + (b >= row)) )
		if a == col:
			new_points.add( (a, b + (b >= row)) )
		if b == row:
			new_points.add( (a + (a >= col), b) )
		if a == col and b == row:
			new_points.add( (a,b) )
	return new_points

def shift_and_lift_bottommost(col, row, points):
	new_points = set()
	for a,b in points:
		if b >= row:
			new_points.add( (a + (a >= col), b + 1) )
			if a == col:
				new_points.add( (a, b + 1) )
			elif b == row:
				new_points.add( (a + (a >= col), b) )
	return new_points

def get_offset_and_scale(rect, n):
		xoffs = 0
		yoffs = 0
		xscale = 1
		yscale = 1
		if rect:
			(x1,y1), (x2,y2) = rect
			xoffs = min(x1,x2)
			yoffs = min(y1,y2)
			xscale = (max(x1,x2) - xoffs) / (n+1)
			yscale = (max(y1,y2) - yoffs) / (n+1)
		return xoffs, yoffs, xscale, yscale

def offset_and_scale_list(xoffs, yoffs, xscale, yscale, lis):
	return map( lambda pnt: (xoffs + xscale * pnt[0], yoffs + yscale * pnt[1] ), lis )
		
def normalize_rect(rect):
	(x1,y1),(x2,y2) = rect
	lef,rig = min(x1,x2), max(x1,x2)
	bot,top = min(y1,y2), max(y1,y2)
	return (lef,bot), (top,rig)

def is_ascending(perm):
	if len(perm) < 2:
		return True
	return all( a < b for a,b in zip(perm,perm[1:]) )
	
def is_descending(perm):
	if len(perm) < 2:
		return True
	return all( a > b for a,b in zip(perm,perm[1:]) )

def show_multiple(patts,width,size=1,**kwargs):
	g = Graphics()
	SEP = 1 * size
	patt_width = max(map(len,patts)) * size
	rows = ceil(len(patts) / width)
	xpos = 0
	ypos = (rows + 2) * (patt_width * SEP)
	for i,p in enumerate(patts):
		if i % width == 0:
			xpos = 0
			ypos -= patt_width + SEP

		g += p._get_graphics_object( rect=[(xpos,ypos),(xpos+patt_width,ypos-patt_width)] )
		
		xpos += patt_width + SEP
	
	if 'figsize' not in kwargs:
		kwargs['figsize'] = max(min(width,len(patts)) * (patt_width + SEP) - SEP, rows * (patt_width + SEP) - SEP)
	show(g,axes=False,aspect_ratio=1,**kwargs)
	
def patt_latex(patt, scale=1):
    desc = ','.join( '%d/%d'%(a,b) for a,b in G(patt) )
    shades = ','.join( '%d/%d'%(a,b) for a,b in patt._shades )
    if isinstance(patt, DecoratedPattern):
        decs = []
        for points, _ in patt._decs:
            xs = [ x for x,_ in points ]
            ys = [ y for _,y in points ]
            decs.append(r'%d/%d/%d/%d/{\onetwo}'%(min(xs),min(ys),max(xs)+1,max(ys)+1))
        decorations = ','.join(decs)
        return r'\decpattern{scale=%f}{%d}{%s}{%s}{}{%s}'%(scale,len(patt),desc,shades,decorations)
    else:
        return r'\mpattern{scale=%f}{%d}{%s}{%s}'%(scale,len(patt),desc,shades)

class ClassicalPattern(Permutation_class):
	def __init__(self, perm):
		if sorted(perm) != [1..len(perm)]:
			raise Exception("Pattern should contain the the elements 1,2,...,n")
		Permutation_class.__init__(self, list(perm))

	def __repr__(self):
		return str(self)
		
	def __str__(self):
		return 'ClassicalPattern(%s)'%(str(self._list))
	
	def _get_graphics_object(self, rect=None):
		n = len(self)
		xo, yo, xs, ys = get_offset_and_scale(rect, n)
			
		L = Graphics()
		
		# Drawing the lines
		for i in [1..n]:
			L += line2d( offset_and_scale_list(xo, yo, xs, ys,[(i,0),(i,n+1)]), rgbcolor=(0,0,0), thickness = 2 * min(xs, ys, 1))
			L += line2d( offset_and_scale_list(xo, yo, xs, ys,[(0,i),(n+1,i)]), rgbcolor=(0,0,0), thickness = 2 * min(xs, ys, 1))
		
		# Drawing the dots
		for i in [1..n]:
			L += point2d( offset_and_scale_list(xo, yo, xs, ys,[(i,self._list[i-1])]), color = 'black', size = 200 * min(xs,ys,1))
		
		return L
	
	def _sanity_check(self):
		pass
	
	def _update_hash(self):
		pass
	
	def _update_after_modification(self):
		self._sanity_check()
		self._update_hash()

	def _occurrences(self, perm, stop_if_found=False):
		"""
		Returns False if self occurs in perm, otherwise True
		"""
		occurrs = []
		
		k = len(self)
		n = len(perm)
	
		if k > n: 
			return []
		
		pat = self._list
		for subw in Subwords(G(perm), k):
			H = G_inv(subw)
			sort_subw = sorted(H)
			if H == [ sort_subw[i-1] for i in pat ]:
				occurrs.append( [ i for i,_ in subw ] )
				if stop_if_found:
					break

		return occurrs

	def show(self, *args, **kwargs):
		if 'aspect_ratio' not in kwargs:
			kwargs['aspect_ratio'] = 1
		self._get_graphics_object().show(*args, **kwargs)

	def copy(self):
		return ClassicalPattern(self._list)
		
	def is_classical(self):
		return True
		
	def contained_in(self, perm):
		"""
		Returns True if self occurs in perm, otherwise False
		"""
		return not self.avoided_by(perm)

	def avoided_by(self, perm):
		return len(self._occurrences(perm, stop_if_found=True)) == 0
		
	def occurrences(self, perm):
		return self._occurrences(perm)
		
	def count_occurrences(self, perm):
		return len(self.occurrences(perm))

	def rect_contains_elements(self,rect):
		(lef,bot), (top,rig) = normalize_rect(rect)
		if rig - lef < 2 or top - bot < 2:
			return False
		for y in self[lef:rig-1]:
			if bot < y < top:
				return True
		
		return False
		
	def get_elements_in_rect(self,rect):
		(lef,bot), (top,rig) = normalize_rect(rect)
		if rig - lef < 2 or top - bot < 2:
			return []
		return [ (x,y) for x,y in G(self)[lef:rig-1] if bot < y < top ]
	
	def insert(self, col, row):
		n = len(self)
		
		if col > n or row > n:
			raise IndexError
		
		raised = map(lambda x: x + (x > row), self._list)
		raised.insert(col, row + 1)
		return ClassicalPattern(raised)
	
	def add_shading(self, shadings):
		if type(self) is ClassicalPattern:
			return MeshPattern(self, shadings)
		
		patt = self.copy()
		patt._shades.update(set(shadings))
		patt._update_after_modification()
		return patt
	
	def add_marking(self, points, count):
		if isinstance(self, MarkedMeshPattern):		
			patt = self.copy()
			pts = set(points).difference(self._shades)
			if pts:
				patt._marks.append( (pts, count) )
				patt._update_after_modification()
				return patt
			
			raise Exception("Attemtping to add an emtpy marking or mark over a completely shaded region")
		else:
			return MarkedMeshPattern(self, marks=[(points,count)])
		
		
	def add_decoration(self, points, pattern,copy=True):
		if isinstance(self, DecoratedPattern):
			if copy:
				patt = self.copy()
			else:
				patt = self
			pts = set(points).difference(self._shades)
			if pts:
				patt._decs.append( (pts, pattern) )
				patt._update_after_modification()
			return patt
		else:
			return DecoratedPattern(self, decs=[(points,pattern)])
		
		
	def add_ascending_restriction(self, rect):
		"""
		The elements of self in the given rectangle, rect, must be
		in descending order
		"""
		(lef,bot), (top,rig) = normalize_rect(rect)
		
		sub_perm = self.get_elements_in_rect(rect)
		if not is_descending(G_inv(sub_perm)):
			raise Exception("The elements in rect must be in descending order")
		
		sh = set()
		for x,y in sub_perm:
			sh.update( set( [(a,b) for a in range(x,rig) for b in range(y,top) ] ) )
			sh.update( set( [(a,b) for a in range(lef,x) for b in range(bot,y) ] ) )
		
		extended = [(lef,top)] + sub_perm + [(rig,bot)]
		
		decs = []
		
		for (x1,y1),(x2,y2) in zip(extended,extended[1:]):
			decs.append( ( set([ (x,y) for x in range(x1,x2) for y in range(y2,y1) ]) , ClassicalPattern([1,2]) ) )
		
		if type(self) is ClassicalPattern:
			return DecoratedPattern( self, shades=sh, decs=decs )
			
		copy = self.copy()
		copy._shades.update( sh )
		
		for pnts,_ in decs:
			pnts.difference_update( copy._shades )
		
		decs = filter( lambda x: x[0], decs )
		
		if type(self) is DecoratedPattern:
			copy._decs += decs
			copy._updated_after_modification()
			return copy
		
		return DecoratedPattern(copy, decs=decs)
	
	def reverse(self):	
		return ClassicalPattern(Permutation_class.reverse(self))
		
	def inverse(self):
		return ClassicalPattern(Permutation_class.inverse(self))
				
	def complement(self):
		return ClassicalPattern(Permutation_class.complement(self))

class MeshPattern(ClassicalPattern):
	def __init__(self, patt=[], shades=[], **kwargs):
		super(MeshPattern, self).__init__(patt)
		if isinstance(patt, MeshPattern):
			self._shades = set(patt._shades)
		else:
			self._shades = set(shades)
		self._update_hash()
		
	shaded_boxes = property(fget = lambda self: self._shades)

	def __repr__(self):
		return str(self)
		
	def __str__(self):
		return 'MeshPattern(%s,%s)'%(str(self._list), str(self._shades))

	def __eq__(self, other):
		if type(self) != type(other) or self._hash != other._hash:
			return False

		if ClassicalPattern.__ne__(self, other):
			return False
		
		return self._shades == other._shades
		
	def __ne__(self, other):
		return not (self == other)
		
	def __lt__(self, other):
		if not isinstance(other, MeshPattern):
			raise TypeError("Cannot compare MeshPattern to other types")
		if Permutation_class.__lt__(self,other):
			return True
		if Permutation_class.__eq__(self,other):
			return self._shade_number() < other._shade_number()
		return False
		
	def __gt__(self, other):
		return other < self
	
	def __le__(self, other):
		return self < other or self == other
	
	def __ge__(self, other):
		return self > other or self == other
	
	def __hash__(self):
		return self._hash
		
	def _update_hash(self):
		self._hash = hash(str(self._list)).__xor__(hash(tuple(sorted(self._shades))))
		
	def _shade_number(self):
		return sum( ( (a,b) in self._shades ) * (1 << ((n+1)*a + b)) for a in [0..n] for b in [0..n] )
	
	def _get_graphics_object(self, rect=None):
		n = len(self)
		xo, yo, xs, ys = get_offset_and_scale(rect, n)
		
		L = super(MeshPattern, self)._get_graphics_object(rect)
		
		# Drawing the shading
		for x,y in self._shades:
			L += polygon2d( offset_and_scale_list(xo, yo, xs, ys,[(x,y), (x+1,y), (x+1,y+1), (x,y+1)]), alpha = 0.5, rgbcolor=(1,1/4,1/2), zorder = -1)	   

		return L
	
	def _pattern_specific_avoidance(self, X, Y, external):
		for x,y in external:
			if (X[x],Y[y]) in self._shades:
				return True
		
		return False 
	
	def _occurrences(self, perm, stop_if_found=False):
		"""
		Returns False if self occurs in perm, otherwise True
		"""
		occurrs = []
		
		k = len(self)
		n = len(perm)
	
		if k > n:
			return []
		
		pat = self._list
		perm_graph = G(perm)
		for subw in Subwords(perm_graph, k):
			H = G_inv(subw)
			sort_subw = sorted(H)
			
			# If the underlying classical pattern matches
			if H == [ sort_subw[i-1] for i in pat ]:
				c = 0
				r = 0
				X = dict()
				Y = dict()

				for i in (1..n):
					if c < k and perm[i-1] == H[c]:
						c += 1
					else:
						X[i] = c
						
					if r < k and i == sort_subw[r]:
						r += 1
					else:
						Y[i] = r
				
				external = [ (i,h) for i,h in perm_graph if h not in H ]
				
				if not self._pattern_specific_avoidance(X,Y,external):
					occurrs.append( [i for i,_ in subw] )
					if stop_if_found:
						break
		return occurrs

	def copy(self):
		return MeshPattern(self)
		
	def is_shaded(self, *args):
		"""
		is_shaded(col,row) - 
			Checks if box indexed by (col,row) is shaded
		is_shaded(list_of_points) -
			Checks if the boxes indexed by all the points in 
			list_of_points are shaded
		"""
		if len(args) == 2:
			col,row = args
			if col > len(self) or row > len(self):
				raise IndexError
			return (col,row) in self._shades
		elif len(args) == 1:
			return set(args[0]).issubset(self._shades)
		
		raise ValueError
		
	def old_avoided_by(self, perm):
		"""
		Returns False if self occurs in perm, otherwise True
		"""
		k = len(self)
		n = len(perm)

		if k > n: 
			return True
		
		pat = G(self._list)
		perm = G(perm)
		for H in Subwords(perm, k):
			X = dict(G(i for (i,_) in H))
			Y = dict(G(sorted(j for (_,j) in H)))
			if H == [ (X[i], Y[j]) for (i,j) in pat ]:
				X[0], X[k+1] = 0, n+1
				Y[0], Y[k+1] = 0, n+1
				shady = ( X[i] < x < X[i+1] and Y[j] < y < Y[j+1]
						  for (i,j) in self._shades
						  for (x,y) in perm
						  )
				if not any(shady):
					return False
		return True
	
	def is_classical(self):
		return not self._shades
	
	def insert(self, col, row):
		n = len(self)
		
		if (col,row) in self._shades:
			raise Exception("Cannot insert into a shaded box")
		
		raised = super(MeshPattern, self).insert(col,row)
		shades = shift_and_lift(col, row, self._shades)
		return MeshPattern(raised, shades)
	
	def reverse(self):	
		n = len(self)
		perm = Permutation_class.reverse(self)
		shades = set( (n-x,y) for (x,y) in self._shades )
		return MeshPattern(perm, shades)
		
	def inverse(self):
		perm = Permutation_class.inverse(self)
		shades = set( (y,x) for (x,y) in self._shades )
		return MeshPattern(perm,shades)
		
	def complement(self):
		n = len(self)
		perm = Permutation_class.complement(self)
		shades = set( (x,n-y) for (x,y) in self._shades )
		return MeshPattern(perm,shades)
		
	def next(self):
		n = len(self)
		shade_n = sum( ( (a,b) in self._shades ) * (1 << ((n+1)*a + b)) for a in [0..n] for b in [0..n] )
		shade_n += 1
		if shade_n == (1 << (n+1)**2):
			if Permutation_class.next(self):
				return MeshPattern(Permutation_class.next(self), set())
			return False
		shades = set( (a,b) for a in [0..n] for b in [0..n] if ((1 << ((n+1)*a + b)) & shade_n) != 0 )
		return MeshPattern(self._list, shades)
		
	def prev(self):
		n = len(self)
		shade_n = sum( ( (a,b) in self._shades ) * (1 << ((n+1)*a + b)) for a in [0..n] for b in [0..n] )
		shade_n -= 1
		if mark_n < 0:
			if Permutation_class.prev(self):
				return MeshPattern(Permutation_class.prev(self), set( (a,b) for a in [0..n] for b in [0..n] ))
			return False
		shades = set( (a,b) for a in [0..n] for b in [0..n] if ((1 << ((n+1)*a + b)) & shade_n) != 0 )
		return MeshPattern(self._list, shades)
		
def MeshPatterns(n):
	p = MeshPattern([1..n])
	while p:
		yield p
		p = p.next()

class MarkedMeshPattern(MeshPattern):
	"""
	INPUT:
	
	-  A MeshPattern
	
	-  A Permuation (or an object that can be passed
	   to the Permutation constructor)

	-  A permutation, and a list of boxes to be shaded
	
	-  A permutation, a list of boxes to be shaded and 
	   a list of markings
	"""
	def __init__(self, *args, **kwargs):
		marks = []
		if 'patt' in kwargs:
			self._list = list(kwargs['patt'])
		
		self._marks = []
		
		if len(args) == 1:
			if isinstance(args[0], MarkedMeshPattern):
				marks = args[0]._marks
			super(MarkedMeshPattern, self).__init__(*args, **kwargs)
		if len(args) >= 2:
			a1, a2 = args[:2]
			if isinstance(a1, MeshPattern):			
				super(MarkedMeshPattern, self).__init__(a1)
				marks = a2
			else:
				super(MarkedMeshPattern, self).__init__(a1, a2)
		if len(args) == 3:
			marks = args[2]
		
		if 'shades' in kwargs:
			self._shades.update( set(kwargs['shades']) )
			
		if 'marks' in kwargs:
			marks += kwargs['marks']
		
		if marks:
			self._marks = map( lambda x: (set(x[0]), x[1]), marks )
		for mark, n in self._marks:
			mark.difference_update(self._shades)
			if not mark:
				raise Exception("Attempting to mark a completely shaded region")
		
		self._update_hash()
		
	def __repr__(self):
		return str(self)
		
	def __str__(self):
		return 'MarkedMeshPattern(%s,%s)'%(super(MarkedMeshPattern, self).__str__(), str(self._marks))
	
	markings = property(fget = lambda self: self._marks)
	
	def __eq__(self,other):
		if type(self) != type(other) or not MeshPattern.__eq__(self,other):
			return False
		return self._marking_list() == other._marking_list()

	def _update_hash(self):
		super(MarkedMeshPattern, self)._update_hash()
		if self._marks:
			self._hash = self._hash.__xor__(hash(str(self._marking_list())))

	def _get_graphics_object(self, rect=None):
		L = super(MarkedMeshPattern, self)._get_graphics_object(rect)
		xo, yo, xs, ys = get_offset_and_scale(rect, len(self))
		
		# Draw marked boxes
		for points, n in self._marks:
			r,g,b = random(),random(),random()
			for x,y in points:
				L += polygon2d( offset_and_scale_list(xo, yo, xs, ys,[(x,y), (x+1,y), (x+1,y+1), (x,y+1)]), alpha = 0.3, rgbcolor=(r,g,b), zorder=-1)
				L += text( str(n), (xo + (x + 0.5) * xs, yo + (y + 0.5) * ys), fontsize=(200 // len(self) * min(xs,ys,1)), rgbcolor=(0,0,0) )

		return L
	
	def _pattern_specific_avoidance(self, X, Y, external):
		if super(MarkedMeshPattern, self)._pattern_specific_avoidance(X,Y,external):
			return True
		
		marks_arr = [ num for (_,num) in self._marks ]
		for i,(m,_) in enumerate(self._marks):
			for x,y in external:
				if (X[x],Y[y]) in m:
					marks_arr[i] -= 1
		
		if all( map( lambda x: x <= 0, marks_arr ) ):
			return False
			
		return True
	
	def _sanity_check(self):
		for mark, n in self._marks:
			if mark.issubset(self._shades):
				raise Exception("Marked region cannot be completely included in a shaded region")
				
	def _marking_list(self):
		return sorted( map( lambda pair: (sorted(pair[0]), pair[1]), self._marks ) )
	
	def copy(self):
		return MarkedMeshPattern(self)
		
	
	def expand(self, follow = None):
		"""
		follow -
			A pair of points defining a rectangle inside the pattern.
			If this argument is not None, the function will return a list of
			tuples, containing a pattern, and the position of the original 
			rectangle in the expanded pattern.
		"""
		if follow:
			(left,bottom),(top,right) = normalize_rect(follow)
		
		if not self._marks:
			if not follow:
				return [ MeshPattern(self) ]
			else:
				return [ (MeshPattern(self), ( (left,bottom), (right,top) )) ]
		else:
			res = []
			points, n = self._marks[0]
			
			for x,y in points:
				if follow:
					n_l = left + (x < left)
					n_r = right + (x < right)
					n_b = bottom + (y < bottom)
					n_t = top + (y < top)
				pat = self.insert(x,y,bottommost=True)
				if follow:
					res += pat.expand(follow=((n_l,n_b),(n_r,n_t)))
				else:
					res += pat.expand()
		return res

	def insert(self, col, row, bottommost=False):
		inserted = super(MarkedMeshPattern, self).insert(col,row)
		
		new_marks = []
		for points, count in self._marks:
			if (col,row) in points:
				if count > 1:
					if bottommost:
						new_marks.append( (set(shift_and_lift_bottommost(col, row, points)), count - 1) )
					else:
						new_marks.append( (set(shift_and_lift(col, row, points)), count - 1) )
			else:
				new_marks.append( (set(shift_and_lift(col, row, points)), count) )
				
		return MarkedMeshPattern(inserted, new_marks)
		
	def reverse(self):	
		n = len(self)
		mesh = MeshPattern.reverse(self)
		marks = [ ([ (n-x,y) for (x,y) in mark ], c) for mark, c in self._marks ]
		return MarkedMeshPattern(mesh, marks)
		
	def inverse(self):
		mesh = MeshPattern.inverse(self)
		marks = [ ([ (y,x) for (x,y) in mark ], c) for mark, c in self._marks ]
		return MarkedMeshPattern(mesh, marks)
		
	def complement(self):
		n = len(self)
		mesh = MeshPattern.inverse(self)
		marks = [ ([ (x,n-y) for (x,y) in mark ], c) for mark, c in self._marks ]
		return MarkedMeshPattern(mesh, marks)
		
class DecoratedPattern(MarkedMeshPattern):
	"""
	INPUT:
	-  A DecoratedPattern, MarkedMeshPattern, MeshPattern or ClassicalPattern
	
	-  A Permuation (or an object that can be passed
	   to the Permutation constructor)
	
	-  A permutation, and a list of boxes to be shaded
	
	-  A permutation, a list of boxes to be shaded and 
	   a list of markings
	   
	-  A permutation, a list of boxes to be shaded,
	   a list of markings and a list of decorations
	"""
	def __init__(self, *args, **kwargs):
		decs = []
		if 'decs' in kwargs:
			decs = kwargs['decs']
	
		self._marks = []
		self._decs = []
		
		if len(args) == 1:
			if isinstance(args[0], DecoratedPattern):
				decs = args[0]._decs
			super(DecoratedPattern, self).__init__(*args, **kwargs)
		if len(args) >= 2:
			if isinstance(args[0], MarkedMeshPattern):
				super(DecoratedPattern, self).__init__(args[0])
				decs = args[1]
			else:
				super(DecoratedPattern, self).__init__(*args, **kwargs)
		if len(args) == 4:
			decs = args[3]
			
		if decs:
			self._decs = map( lambda pair: (set(pair[0]).difference(self._shades),pair[1]), decs )
			self._decs = filter( lambda pair: pair[0], self._decs )
		
		self._update_hash()
		
	def __repr__(self):
		return str(self)
		
	def __str__(self):
		return 'DecoratedPattern(%s,%s)'%(super(DecoratedPattern, self).__str__(), str(self._decs))
		
	def __eq__(self, other):
		if type(self) != type(other) or not MarkedMeshPattern.__eq__(self,other):
			return False
		return self._decoration_list() == other._decoration_list()
	
	decorations = property(fget = lambda self: self._decs)
	
	def _update_hash(self):
		super(DecoratedPattern, self)._update_hash()
		if self._marks:
			self._hash = self._hash.__xor__(hash(str(self._decoration_list())))
	
	def _get_graphics_object(self, rect=None):
		L = super(DecoratedPattern, self)._get_graphics_object(rect)
		
		xo, yo, xs, ys = get_offset_and_scale(rect, len(self))
		
		for points, pat in self._decs:
			r,g,b = random(),random(),random()
			for x,y in points:
				L += polygon2d( offset_and_scale_list(xo, yo, xs, ys, [(x,y), (x+1,y), (x+1,y+1), (x,y+1)]), alpha = 0.2, rgbcolor=(r,g,b), zorder=-1)
				if pat is self:
					L += text( str('...'), (xo + (x + 0.5) * xs, yo + (y + 0.5) * ys), fontsize=(150 // len(self) * min(xs,ys)), rgbcolor=(0,0,0) )
				else:
					L += pat._get_graphics_object( rect=[ (xo + x * xs, yo + y * ys), (xo + (x+1) * xs, yo + (y+1) * ys) ] )
				
		return L
	
	def _pattern_specific_avoidance(self, X, Y, external):
		if super(DecoratedPattern, self)._pattern_specific_avoidance(X,Y,external):
			return True
		
		for pnts, sub_pat in self._decs:
			extracted = [ row for col,row in external if (X[col],Y[row]) in pnts ]
			if sub_pat.contained_in(to_standard(extracted)):
				return True
		
		return False

	def _decoration_list(self):
		return sorted( map( lambda pair: (sorted(pair[0]), pair[1]), self._decs ) )

	def copy(self):
		return DecoratedPattern(self)

	def is_classical(self):
		return super(DecoratedPattern, self).is_classical() and not self._decs

	def insert(self, col, row):
		inserted = super(DecoratedPattern, self).insert(col,row)		
		new_decs = [ (set(shift_and_lift(col, row, points)), dec) for points, dec in self._decs ]
		return DecoratedPattern(inserted, new_decs)
		
	def reverse(self):	
		n = len(self)
		mesh = DecoratedPattern.reverse(self)
		decs = [ [ (n-x,y) for (x,y) in mark ] for mark, n in self._decs ]
		return MarkedMeshPattern(mesh, marks)
		
	def inverse(self):
		mesh = DecoratedPattern.inverse(self)
		decs = [ [ (y,x) for (x,y) in mark ] for mark, n in self._decs ]
		return MarkedMeshPattern(mesh, marks)
		
	def complement(self):
		n = len(self)
		mesh = DecoratedPattern.inverse(self)
		decs = [ [ (x,n-y) for (x,y) in mark ] for mark, n in self._decs ]
		return MarkedMeshPattern(mesh, marks)
		
