'''
-----------------------------------------------------------------
The function is_simple checks if a permutation is simple.
It calls the subfunction is_contiguous
-----------------------------------------------------------------
'''

def is_simple(w):
    
    n = len(w)
    
    # i = 2 corresponds to an interval of length 2
    for i in xrange(2,n):
        
        for j in xrange(0,n-i+1):
            
            if is_contiguous([w[k] for k in xrange(j,j+i)]):
                #print i,j
                #print [w[k] for k in xrange(j,j+i-1)]
                return False
                
    return True
    
def is_contiguous(lst):
    
    slst = sorted(lst)
    
    for j in xrange(0,len(lst)-1):
        if slst[j+1] - slst[j] != 1:
            return False
            
    return True

'''
-----------------------------------------------------------------
The function number_of_clumps outputs the number of clumps in a permutation - see
http://arxiv.org/abs/1105.3093.
It calls the subfunction is_contiguous
-----------------------------------------------------------------
'''

def max_number_of_clumps(perm):
    
    mc = -1
    
    for d in perm.descents():
        
        c = number_of_clumps(perm,d)
        if c > mc:
            mc = c

    return mc
    
def number_of_clumps(perm,d):
    
    front = sorted(filter(lambda x: x < perm[d] and x > perm[d+1], perm[0:d]))
    back  = sorted(filter(lambda x: x < perm[d] and x > perm[d+1], perm[d+2::]))
    
    c = 0
    
    if front:
        c = c+1
    if back:
        c = c+1
    
    for i in range(len(front)-1):
        
        if front[i+1]-front[i] > 1:
            c = c+1
            
    for i in range(len(back)-1):
        
        if back[i+1]-back[i] > 1:
            c = c+1
    
    return c

'''
-----------------------------------------------------------------
The function returns True on balanced permutations
-----------------------------------------------------------------
'''

def is_balanced(perm):
    
    n = len(perm)
    if n%4 != 0:
        return False

    else:
        return len(filter( lambda x: x <= n/2, map(lambda x: perm(x), [int(n/2+1)..n]) )) == n/4


'''
-----------------------------------------------------------------
The function returns True on menage permutations
-----------------------------------------------------------------
'''
def is_menage(perm):
    
    n = len(perm)
    for i in [0..len(perm)-1]:
        if perm[i]-1 == i:
            return False
        if perm[i]-1 == mod(i+1,n):
            return False
    return True

'''
-----------------------------------------------------------------
The function returns True on Kendall-Mann permutations
-----------------------------------------------------------------
'''
def is_Kendall_Mann(perm):
    n = len(perm)
    return perm.number_of_inversions() < floor(n*(n-1)/4)

'''
-----------------------------------------------------------------
The function returns True on Gorenstein permutations
-----------------------------------------------------------------
'''
def is_Gorenstein(perm):
    mp1 = ([2,4,1,5,3],[(3,0),(3,1),(3,2),(3,3),(3,4),(3,5),(0,2),(1,2),(2,2),(3,2),(4,2),(5,2)])
    mp2 = ([3,1,5,2,4],[(2,0),(2,1),(2,2),(2,3),(2,4),(2,5),(0,3),(1,3),(2,3),(3,3),(4,3),(5,3)])
    if not avoids_mpats(perm,[mp1,mp2]):
        return False

    for gr in Grassmannian_perms(perm):
        if not outer_corners_balanced( associated_partition(gr) ):
            return False

    return all_corners_on_anti_diagonal(perm)

def all_corners_on_anti_diagonal(perm):
    for gr in Grassmannian_perms(perm):
        if not outer_corners_balanced( associated_partition(gr) ):
            return False

    return True

def Grassmannian_perms(perm):

    perm_descents = perm.descents()

    if not perm_descents:
        return []

    Grs = []
    for d in perm_descents:

        def append_if_small(x,y):
            if y < x[-1]:
                return x+[y]
            else:
                return x

        def append_if_big(x,y):
            if y > x[-1]:
                return x+[y]
            else:
                return x

        front = reduce(lambda x,y : append_if_small(x,y), reversed(perm[0:d]),[perm[d]])
        front.reverse()
        back  = reduce(lambda x,y : append_if_big(x,y), perm[d+2:],[perm[d+1]])

        Grs.append(to_standard(front+back))

    return Grs

def associated_partition(gr):
    lattice_path = []
    d = gr.descents()[0]
    for i in [1..len(gr)]:
        if gr.index(i) <= d:
            lattice_path.append(1) #upstep
        else:
            lattice_path.append(0) #rightstep

    return lattice_path

def outer_corners_balanced(lattice_path):

    len_l_path = len(lattice_path)

    height = sum(lattice_path)
    width  =  len_l_path - height

    vert_count = 0
    hori_count = 0

    m = []

    for (i,step) in enumerate(lattice_path):

        if step == 0:
            hori_count = hori_count + 1
        else:
            vert_count = vert_count + 1

            if i < (len_l_path - 1) and lattice_path[i+1] == 0:
                m.append((height - vert_count) + hori_count)

                if Set(m).cardinality() > 1:
                    return False
    return True

'''
-----------------------------------------------------------------
The function calculates the swapping number of a permutation.
See http://www.usna.edu/Users/math/wdj/SM485_2.TXT
-----------------------------------------------------------------
'''

def swapping_number(perm):
    
    def e_func(i,perm):
        m = 0
        for j in [0..i-1]:
            if perm[j] > perm[i]:
                m = m+1
        return m
    
    return sum(map(lambda x : e_func(x,perm) , [0..len(perm)-1]))