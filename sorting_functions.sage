#
# helper function for stack-sort and bubble-sort. finds the location of the maximal element
#

def loc_max(w):
    
    L = max(w)
        
    for i in [0..L-1]:
        if w[i] == L:
            return i
            
#
# function takes a permutation w and does one pass of stack-sort on it
#

def stack_sort(w):
    
    i = len(w)
    
    if i == 0:
        return []
    
    if i == 1:
        return w
    else:
        [j,J] = [loc_max(w),max(w)]
        
        if j == 0:
            W2 = list(stack_sort(w[1:i]))
            W2.append(J)
            
            return W2
            
        if j == i-1:
            W1 = list(stack_sort(w[0:i-1]))
            W1.append(J)
        
            return W1
        
        
        else:
            W1 = list(stack_sort(w[0:j]))
            W2 = list(stack_sort(w[j+1:i]))
        
            W1.extend(W2)
            W1.extend([J])
        
            return W1

#
# function takes a permutation w and does one pass of bubble-sort on it
#
# Lína!

def bubble_sort(w):
    
    i = len(w)
    
    if i == 0:
        return []
    
    if i == 1:
        return w
    else:
        [j,J] = [loc_max(w),max(w)]
        
        if j == 0:
            W2 = w[1:i]
            W2.append(J)
            
            return W2
            
        if j == i-1:
            W1 = list(bubble_sort(w[0:i-1]))
            W1.append(J)
        
            return W1
        
        
        else:
            W1 = list(bubble_sort(w[0:j]))
            W2 = w[j+1:i]
        
            W1.extend(W2)
            W1.extend([J])
        
            return W1

'''
Sorting functions for quick-sorting
First we have my proposal for quick-sorting
'''
def pivot_on_loc(perm,loc):
    
    piv = perm[loc]
    
    frontperm = []
    backperm  = []
    
    R = range(0,loc)
    R.extend(range(loc+1,len(perm)))
    
    for i in R:
        
        if perm[i] < piv:
            frontperm.append(perm[i])
        else:
            backperm.append(perm[i])
            
    frontperm.append(piv)
    frontperm.extend(backperm)
    return Permutation(frontperm)

def is_1_quick_sortable(perm):
    
    n = len(perm)
    idperm = Permutation(range(1,n+1))
    for loc in xrange(0,n):
        
        if pivot_on_loc(perm,loc) == idperm:
            
            return True
        
    return False
    
def is_2_quick_sortable(perm):
    
    n = len(perm)
    idperm = Permutation(range(1,n+1))
    for loc1 in xrange(0,n):
        
        newperm = pivot_on_loc(perm,loc1)
        
        for loc2 in xrange(0,n):
            
            if pivot_on_loc(newperm,loc2) == idperm:
                
                return True
        
    return False

'''
Here is Anders's suggestion
'''
def Q2(w,x):
   j = w.index(x)
   return [ y for (i,y) in enumerate(w) if i<j and y>x ]

def Q4(w,x):
   j = w.index(x)
   return [ y for (i,y) in enumerate(w) if i>j and y<x ]

def SFix(w):
   return [ x for x in w if not Q2(w,x) and not Q4(w,x) ]

def K(w):
   if not w: return w
   S = SFix(w)
   if S:
       x = S[-1]
       return K(w[:x-1]) + [x] + K(w[x:])
   else:
       x = w[0]
       return [ y for y in w if y < x ] + [x] + [ y for y in w if y > x ]