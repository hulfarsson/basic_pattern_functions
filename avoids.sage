
def avoids(permutation, pattern):
    m = len(pattern)

    p_inv = [0] * m
    n = 1
    for v in pattern:
        p_inv[v-1] = n
        n += 1

    for perm in Subwords(permutation, m):
        nm = perm[p_inv[0] - 1]
        for p in p_inv[1:]:
            n = perm[p - 1]
            if nm < n:
                nm = n
            else:
                break
        else:
            return False
    return True

def patt_pos(permutation, pattern):
    m = len(pattern)

    p_inv = [0] * m
    n = 1
    for v in pattern:
        p_inv[v-1] = n
        n += 1

    pos = []
    dct = {}
    for n, i in enumerate(permutation):
        dct[i] = n

    for perm in Subwords(permutation, m):
        nm = perm[p_inv[0] - 1]
        for p in p_inv[1:]:
            n = perm[p - 1]
            if nm < n:
                nm = n
            else:
                break
        else:
            pos.append([dct[i] for i in perm])

    return pos
