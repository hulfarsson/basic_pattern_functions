import random

def to_standard(p, lst=False):
    ps = zip(p, range(1, len(p) + 1))
    ps.sort()
    ret = [0] * len(p)
    n = 1
    for _, v in ps:
        ret[v-1] = n
        n += 1

    if lst:
        return ret
    return Permutation(ret)
